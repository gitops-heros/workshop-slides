let xp = 0
let money = 10

const labelContainer = document.getElementById("label-container")
const labelLevel = document.getElementById("label-level")

function resolveAndGo(link, difficulty, coins = 0) {
    resolve(difficulty, coins)
    location.assign(link)
}
function resolve(difficulty, coins = 0) {
    let lastLevel = getLevel(xp)
    xp += difficulty
    money += coins

    let newLevel = getLevel(xp)

    if (lastLevel < newLevel) {
        // level up
        labelLevel.innerHTML = '💪🆙'
        labelLevel.className = 'show lvl-up';
        labelContainer.className = 'show header-inner'
        setTimeout(function () {
            labelLevel.className = 'hide lvl-up';
        }, 2000);
    } else if (lastLevel > newLevel) {
        // level down
        labelLevel.innerHTML = '🤢🔽'
        labelLevel.className = 'show lvl-down';
        labelContainer.className = 'show header-inner'
        setTimeout(function () {
            labelLevel.className = 'hide lvl-down';
        }, 2000);
    }

    labelContainer.innerHTML = `Level: ${newLevel} (xp: ${xp}), gold: ${money}`
    labelContainer.className = 'show header-inner';

    if (coins != 0) {
        let audio=new Audio('sounds/gold.mp3')
        audio.play()
    }

}


function getLevel(xp) {

    if (xp >= 4100) {
        return 4;
      }
      if (xp >= 1700) {
        return 3;
      }
      if (xp >= 700) {
        return 2;
      }
      if (xp >= 300) {
        return 1;
      }
      return 0;
}
